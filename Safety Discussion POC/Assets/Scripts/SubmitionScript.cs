﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.IO;

public class SubmitionScript : MonoBehaviour {

    //variables to gather the user input for database submittion using input fields

    public Button submit;
    public Button back;

    public InputField dateInputField;
    public InputField locationInputField;
    public InputField submitionInputField;
    public InputField subjectInputField;
    public InputField outcomesInputField;
    public InputField colleagueInputField;

    // Use this for initialization
    void Start () {


        Button buttonsub = submit.GetComponent<Button>();
        buttonsub.onClick.AddListener(TaskOnClick);

        Button buttonbck = back.GetComponent<Button>();
        buttonbck.onClick.AddListener(mainMenuNav);

    }


    void TaskOnClick()
    {
        //Allows for scene navigation
        InputActivation();
        SceneManager.LoadScene("SafeDisplay");
        //TextActivation();
            
        Debug.Log("You have submitted your form");
    }

    void mainMenuNav()
    {
        Debug.Log("Back to the Main Menu");
        SceneManager.LoadScene("Titlepage");
    }

    void InputActivation()
    {
        /* .enabled deactivates the typing but not the actual display of the input on the screen maybe 
        *  make another game object for each and try deactivating them to make them dissapear
        */

        dateInputField.enabled = false;
        locationInputField.enabled = false;
        submitionInputField.enabled = false;
        subjectInputField.enabled = false;
        outcomesInputField.enabled = false;
        colleagueInputField.enabled = false;

        Debug.Log("You can no longer write");

    }
}
