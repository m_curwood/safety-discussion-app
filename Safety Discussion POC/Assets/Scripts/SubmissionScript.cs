﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.IO;
using LitJson;

public class SubmissionScript : MonoBehaviour {

    //variables to gather the user input for database submittion using input fields

    public Button submit;
    public Button back;

    public InputField dateInputField;
    public InputField locationInputField;
    public InputField submissionInputField;
    public InputField subjectInputField;
    public InputField outcomesInputField;
    public InputField colleagueInputField;

    public static string conAddition;
    public string inputAddition;

    public static string userAddition;
    public string userInput;

    public ConversationObject conOb;
    public UserObject userOb;

    public string submissionInput;

    // Use this for initialization
    void Start () {


        Button buttonsub = submit.GetComponent<Button>();
        buttonsub.onClick.AddListener(TaskOnClick);

        Button buttonbck = back.GetComponent<Button>();
        buttonbck.onClick.AddListener(mainMenuNav);
    }

    void TaskOnClick()
    {
        //Allows for scene navigation
        InputActivation();

       // Debug.Log(JsonUserCreator());

        Debug.Log(inputAddition);
        Debug.Log(conAddition);        

        //File Reloader as file will need to be resaved and possibly loaded again
        FileLoader.jsonConSaver();

        SceneManager.LoadScene("SafeDisplay");
        //TextActivation();
            
        Debug.Log("You have submitted your form");
    }

    void mainMenuNav()
    {
        Debug.Log("Back to the Main Menu");
        SceneManager.LoadScene("Titlepage");
    }

    void InputActivation()
    {
        /* .enabled deactivates the typing but not the actual display of the input on the screen maybe 
        *  make another game object for each and try deactivating them to make them dissapear
        */

        conOb = new ConversationObject();
        userOb = new UserObject();

        submissionInput = submissionInputField.text;
        FileLoader.input = submissionInput;

        UserObject.id = FileLoader.getUserId();
        UserObject.firstName = submissionInput.Substring(0,submissionInput.IndexOf(" "));
        UserObject.lastName = submissionInput.Substring(submissionInput.IndexOf(" "));

        //User saver
        userInput = JsonUserCreator();
        userAddition = FileLoader.userString.Substring(0, FileLoader.userString.Length - 2) + "," + userInput + "]}";

        FileLoader.jsonUserSaver();

        //need to reload the Json User File after saving as submission will not display otherwise
        FileLoader.jsonLoader();

        conversationIdFinder();
        ConversationObject.location = locationInputField.text;
        ConversationObject.subject = subjectInputField.text;
        ConversationObject.outcomes = outcomesInputField.text;
        ConversationObject.colleagues = colleagueInputField.text;        

        //Conversation saver
        inputAddition = JsonConCreator();
        conAddition = FileLoader.conversationDatabase.Substring(0, FileLoader.conversationDatabase.Length - 2) + "," + inputAddition + "]}";

        Debug.Log(userOb.ToString());
        //Debug.Log(JsonUserCreator());

        dateInputField.enabled = false;
        locationInputField.enabled = false;
        submissionInputField.enabled = false;
        subjectInputField.enabled = false;
        outcomesInputField.enabled = false;
        colleagueInputField.enabled = false;

        Debug.Log("You can no longer write");

    }

    void conversationIdFinder()
    {
        Debug.Log(FileLoader.users["userData"].Count);

        for (int i = 0; i < FileLoader.users["userData"].Count; i++)
        {
            if (submissionInputField.text.StartsWith(FileLoader.users["userData"][i]["firstName"].ToString()))
            {
                ConversationObject.conId = FileLoader.getUserId() + dateInputField.text;
            }
        }
    }

    public string JsonConCreator()
    {
        return JsonMapper.ToJson(conOb);
    }

    public string JsonUserCreator()
    {
        return JsonMapper.ToJson(userOb);
    }
}

public class ConversationObject
{
    public static string conId;
    public static string location;
    public static string subject;
    public static string outcomes;
    public static string colleagues;

    public ConversationObject()
    {
        
    }

}

public class UserObject
{
    public static string id;
    public static string firstName;
    public static string lastName;

    public UserObject()
    {

    }

}