﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.IO;

public class DisplayScript : MonoBehaviour {

    //used to display user conversations from json Database
    public static Text dateBody;
    public static Text locationBody;
    public static Text submissionBody;
    public static Text subjectBody;
    public static Text outcomesBody;
    public static Text colleagueBody;

    public Button buttonBack;
    public Button buttonSub;

    public GameObject conDisplay;
    public GameObject content;

    public string userId;

    // Use this for initialization
    void Start () {

        dateBody = conDisplay.GetComponentsInChildren<Text>()[0];
        locationBody = conDisplay.GetComponentsInChildren<Text>()[1];
        submissionBody = conDisplay.GetComponentsInChildren<Text>()[2];
        subjectBody = conDisplay.GetComponentsInChildren<Text>()[3];
        outcomesBody = conDisplay.GetComponentsInChildren<Text>()[4];
        colleagueBody = conDisplay.GetComponentsInChildren<Text>()[5];

        conversationDisplayCreation();

        Button buttonbck = buttonBack.GetComponent<Button>();
        buttonbck.onClick.AddListener(mainMenuNav);

        Button buttonSubmit = buttonSub.GetComponent<Button>();
        buttonSubmit.onClick.AddListener(TaskOnClick);

    }

    //creation and formatting of display using instantiation of User Interface Prefabs
    void conversationDisplayCreation()
    {
        Debug.Log(FileLoader.conversations["conversation"].Count);

        int conCount = 0;    

        //for loop to cycle through recorded conversations and display conversations that have been submitted by recorded users
        for (int i = 0; i < FileLoader.conversations["conversation"].Count; i++)
        {     
            //if check to determine which conversations to display will only display conversations related to the User ID 
            if (FileLoader.conversations["conversation"][i]["conId"].ToString().StartsWith(FileLoader.getUserId()))
            {
                Instantiate(conDisplay, new Vector3(Vector3.down.x + 432.5f, i * (Vector3.down.x - 200), 0), Quaternion.identity, conDisplay.transform.parent);

                conDisplay.tag = "conDis/" + conCount;
                Debug.Log(conDisplay.name);
                Debug.Log(conDisplay.tag);

                dateBody = conDisplay.GetComponentsInChildren<Text>()[0];
                locationBody = conDisplay.GetComponentsInChildren<Text>()[1];
                submissionBody = conDisplay.GetComponentsInChildren<Text>()[2];
                subjectBody = conDisplay.GetComponentsInChildren<Text>()[3];
                outcomesBody = conDisplay.GetComponentsInChildren<Text>()[4];
                colleagueBody = conDisplay.GetComponentsInChildren<Text>()[5];

                dateBody.text = FileLoader.conversations["conversation"][i]["conId"].ToString();
                locationBody.text = FileLoader.conversations["conversation"][i]["location"].ToString();
                subjectBody.text = FileLoader.conversations["conversation"][i]["subject"].ToString();
                submissionBody.text = FileLoader.getUserNames();
                outcomesBody.text = FileLoader.conversations["conversation"][i]["outcomes"].ToString();
                colleagueBody.text = FileLoader.conversations["conversation"][i]["colleagues"].ToString();

                conCount++;
            }

        }
    }

    void mainMenuNav()
    {
        Debug.Log("Back to the Main Menu");
        SceneManager.LoadScene("Titlepage");
    }

    void TaskOnClick()
    {
        //Allows for scene navigation
        Debug.Log("ready for submission");
        SceneManager.LoadScene("UserInput");

    }
}
