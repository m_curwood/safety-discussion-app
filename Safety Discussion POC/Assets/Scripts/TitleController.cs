﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TitleController : MonoBehaviour {

    public Button displayButton;
    public Button submitButton;
    public Button EnterButton;
    public Image popUpDisplay;

    public Text ErrorMessage;
    public Text UIDTitle;

    public InputField userInput;
    public InputField userIdInput;

    

	// Use this for initialization
	void Start ()
    {
        //Disables Pop up field and error messages
        EnterButton.gameObject.SetActive(false);
        UIDTitle.enabled = false;
        userIdInput.enabled = false;
        userIdInput.placeholder.enabled = false;
        userIdInput.image.enabled = false;
        ErrorMessage.enabled = false;
        popUpDisplay.enabled = false;

        Button buttonEnter = EnterButton.GetComponent<Button>();
        buttonEnter.onClick.AddListener(enterButtonClick);
    }

    public void displayButtonClick()
    {
        FileLoader.input = userInput.text;
        Debug.Log(FileLoader.input.Contains(FileLoader.getUserNames()));
        Debug.Log(FileLoader.input);

        //user input needs fixing and input comparison may need static fields from file loader
        if (FileLoader.input.Contains(FileLoader.getUserNames()))
        {            
            Debug.Log("Entering Display");
            SceneManager.LoadScene("SafeDisplay");
        }
        else if(userInput.text == null)
        //there are two names of the same name the user ID pop up will activate
        {
            popUpDisplay.enabled = true;
            UIDTitle.enabled = true;
            userIdInput.enabled = true;
            EnterButton.gameObject.SetActive(true);
        }
        else
        //Pop up says no User Input
        {
            popUpDisplay.enabled = true;
            ErrorMessage.enabled = true;
            EnterButton.gameObject.SetActive(true);
        }        
    }

    public void enterButtonClick()
    {
        popUpDisplay.enabled = false;
        ErrorMessage.enabled = false;
        EnterButton.gameObject.SetActive(false);
    }

    //submit button OnClick() used for entering the user conversation submitter
    public void submitButtonClick()
    {
        Debug.Log("Entering Submission");
        SceneManager.LoadScene("UserInput");
    }

}
